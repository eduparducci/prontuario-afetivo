import uuid
from django.db import models

class MedicalRecord(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    patient_name = models.CharField(max_length=100)
    description = models.TextField(max_length=300)

    def __str__(self):
        return self.name

class Question(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    patient_id = models.ForeignKey('MedicalRecord', on_delete=models.CASCADE, related_name='questions')
    title = models.TextField(max_length=300)
    answer = models.TextField(max_length=300)

    def __str__(self):
        return self.title


