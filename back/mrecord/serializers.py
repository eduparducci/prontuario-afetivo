from rest_framework import serializers
from .models import MedicalRecord, Question

class MedicalRecordSerializer(serializers.ModelSerializer):

    class Meta:
        model = MedicalRecord
        fields = "__all__"
        depth = 1
        
class QuestionSerializer(serializers.Serializer):
    class Meta:
        model = Question
        fields = "__all__"
