import json
from rest_framework import generics, renderers, status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import MedicalRecord, Question
from .serializers import MedicalRecordSerializer, QuestionSerializer


class ListMedicalRecords(generics.ListCreateAPIView):
    """
    List all medical records in the database
    """
    queryset = MedicalRecord.objects.all()
    serializer_class = MedicalRecordSerializer

class CreateMedicalRecord(APIView):
    """
    Create a new medical record
    """
    def post(self, request, format=None):

        record = json.loads(request.body)
        
        if not ('patient_name' in record and 'description' in record):
            msg = {'error': 'missing arguments for medical record'}
            return Response(msg, status=status.HTTP_406_NOT_ACCEPTABLE)

        else:
            new_record = MedicalRecord.objects.create(
                patient_name = record['patient_name'],
                description = record['description']
            )
            return Response(
                MedicalRecordSerializer(new_record).data,
                status=status.HTTP_201_CREATED
            )
